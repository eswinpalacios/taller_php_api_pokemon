<?php
    include "PokeApi.php";
    use PokePHP\PokeApi;
    
    $api = new PokeApi;
    $pokemon_json = $api->pokemon('pikachu');
    $pokemon_obj = json_decode($pokemon_json); //devuelve un obj
    //$pokemon_array = json_decode($pokemon_json, true); //devuelve un array
    //echo $pokemon_obj->name; //forma de utilizar la informacion de un objeto 
    //echo $pokemon_array['name']; //forma de utilizar la informacion de un array 

    function abilities($abilities) //para obtener las habilidades en una cadena
    {
    	$_abilities = "";

    	foreach ($abilities as $ability) {
    		$_abilities .= $ability->ability->name." ";
    	}

    	return $_abilities;
    }

    function types($types)  //para obtener los tipos en una cadena
    {
    	$_types = "";

    	foreach ($types as $type) {
    		$_types .= $type->type->name." ";
    	}

    	return $_types;
    }

    function img($img)
    {		
		return $img->front_default;
    }
?>