<?php include "fuente.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Pokemon Pokedex</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">
		main{
			margin-top: 20px;
		}
		figure{
			border: 5px solid #B0E2FF;
			border-radius: 10px;
		}
	</style>
	
</head>
<body>

<main>

<section class="container">

	<img src="pokemon-go-logo.png" alt="logo de Pokemon">

	<article>
		
		<div class="row">
			<div class="col-md-4">
				<h3><?php echo strtoupper($pokemon_obj->name); ?></h3>
			</div>
		</div>

		<div class="row">

			<div class="col-md-4">
				<figure>
					<img src="https://img.pokemondb.net/artwork/<?php echo $pokemon_obj->name; ?>.jpg">
				</figure>			
			</div>

			<div class="col-md-5">
			  
					<form class="form-horizontal">

					  <div class="form-group">
					    <label class="col-sm-2 control-label">#</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo $pokemon_obj->id; ?>" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">name</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo $pokemon_obj->name; ?>" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">abilities</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo abilities($pokemon_obj->abilities); ?>" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">types</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo types($pokemon_obj->types); ?>" readonly>
					    </div>
					  </div>	  

					  <div class="form-group">
					    <label class="col-sm-2 control-label">Height</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo $pokemon_obj->height; ?> cm" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">Weight</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="<?php echo $pokemon_obj->weight/10; ?> kg" readonly>
					    </div>
					  </div>
					</form>

			</div>
		</div>

	</article>

</section>

</main>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>