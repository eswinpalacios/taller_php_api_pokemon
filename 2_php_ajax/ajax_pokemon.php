<?php
    include "PokeApi.php";
    use PokePHP\PokeApi;
    
    if(isset($_POST['name']))
    	$name = $_POST['name'];
    else
    	$name = "pikachu";

    $api = new PokeApi;

    $pokemon_json = $api->pokemon($name);
    $pokemon_obj = json_decode($pokemon_json);
    
    //opt1
    //$pokemon_obj->abilities = abilities($pokemon_obj->abilities);
    //$pokemon_obj->types = types($pokemon_obj->types);
    //$pokemon_obj->img = "https://img.pokemondb.net/artwork/$pokemon_obj->name.jpg";

    //echo json_encode($pokemon_obj);

    //opt2
    $obj = new stdClass;
    $obj->abilities = abilities($pokemon_obj->abilities);
    $obj->height = $pokemon_obj->height;
    $obj->img = "https://img.pokemondb.net/artwork/$pokemon_obj->name.jpg";
    $obj->name = $pokemon_obj->name;
    $obj->order = $pokemon_obj->order;
    $obj->types = types($pokemon_obj->types);
    $obj->weight = $pokemon_obj->weight;

    echo json_encode($obj);

    function abilities($abilities)
    {
    	$_abilities = "";

    	foreach ($abilities as $ability) {
    		$_abilities .= $ability->ability->name." ";
    	}

    	return $_abilities;
    }

    function types($types)
    {
    	$_types = "";

    	foreach ($types as $type) {
    		$_types .= $type->type->name." ";
    	}

    	return $_types;
    }

    function img($img)
    {		
		return $img->front_default;
    }
?>