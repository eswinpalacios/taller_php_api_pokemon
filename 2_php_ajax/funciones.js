//ready	http://api.jquery.com/ready/
//clic	https://api.jquery.com/click/
//post https://api.jquery.com/jquery.post/
//loading.gif http://preloaders.net/en/free

$( document ).ready(function() {

	$( "#btn-buscar" ).click(function() {
	  	buscar();
	});	

});

function buscar()
{
	console.log("buscar");

	$("#loading").show();

	$.post( "ajax_pokemon.php", { name: $("#txt-name").val() }, function( data ) {
	  	var _data = JSON.parse(data);

	  	$("#abilities").val(_data.abilities);
	  	$("#height").val(_data.height);
	  	$("#name").val(_data.name);
	  	$("#order").val(_data.order);
	  	$("#types").val(_data.types);
	  	$("#weight").val(_data.weight);
	  	$('#img').attr('src',_data.img);
	  	$('#name_title').html(_data.name);
	  	
	}).always(function() {
    	$("#loading").hide();
	});	
}
