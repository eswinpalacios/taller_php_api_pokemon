<!DOCTYPE html>
<html>
<head>
	<title>Pokemon Pokedex</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">
		main{
			margin-top: 20px;
		}
		figure{
			border: 5px solid #B0E2FF;
			border-radius: 10px;
		}
	</style>
	
</head>
<body>

<main>

<section class="container">

	<img src="pokemon-go-logo.png" alt="logo de Pokemon">

	<div class="row">
		<div class="col-md-1" >
			<label>Nombre :</label>
		</div>
		
		<div class="col-md-2" >
			<input type="text" id="txt-name" class="form-control" placeholder="nombre" value="pikachu">
		</div>

		<div class="col-md-2" >
			<button id="btn-buscar" class="form-control btn-primary">buscar</button>
		</div>

		<div class="col-md-1" style="display:none" id="loading">
			<img  src="loading.gif">
		</div>
	</div>	

	<article>
		
		<div class="row">
			<div class="col-md-4">
				<h3><div id="name_title"></div></h3>
			</div>
		</div>

		<div class="row">

			<div class="col-md-4">
				<figure>
					<img id="img" src="">
				</figure>			
			</div>

			<div class="col-md-5">
			  
					<form class="form-horizontal">

					  <div class="form-group">
					    <label class="col-sm-2 control-label">#</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="order" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">name</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="name" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">abilities</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="abilities" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">types</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="types" readonly>
					    </div>
					  </div>	  

					  <div class="form-group">
					    <label class="col-sm-2 control-label">height</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="height" readonly>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">weight</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" id="weight" readonly>
					    </div>
					  </div>
					</form>

			</div>
		</div>

	</article>

	<div class="row" id="result"></div>

</section>

</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="funciones.js" ></script>

</body>
</html>