//ready	http://api.jquery.com/ready/
//clic	https://api.jquery.com/click/
//post https://api.jquery.com/jquery.post/
//loading http://preloaders.net/en/free
//get https://api.jquery.com/jquery.get/

$( document ).ready(function() {

	$( "#btn-buscar" ).click(function() {
	  	buscar();
	});	

	$( "#btn-buscar-directo" ).click(function() {
	  	buscar_directo();
	});

});

function buscar()
{
	console.log("buscar");
	$("#loading").show();

	$.post( "getpokemon", { name: $("#txt-name").val() }, function( data ) {
	  	var _data = JSON.parse(data);

	  	$("#abilities").val(_data.abilities);
	  	$("#height").val(_data.height);
	  	$("#name").val(_data.name);
	  	$("#order").val(_data.order);
	  	$("#types").val(_data.types);
	  	$("#weight").val(_data.weight);
	  	$('#img').attr('src',_data.img);
	  	$('#name_title').html(_data.name);
	  	
	}).always(function() {
    	$("#loading").hide();
	});	
}

function buscar_directo()
{
	console.log("buscar_directo");
	$("#loading").show();

	var pokemon = $("#txt-name").val();
	pokemon = pokemon.toLowerCase();
	var url = "http://pokeapi.co/api/v2/pokemon/" + pokemon;

	$.get( url, function( data ) {  	
  		//var _data = JSON.parse(data);  		
		$("#abilities").val(abilities(data.abilities));
		$("#types").val(types(data.types));
	  	$("#height").val(data.height);
	  	$("#name").val(data.name);
	  	$("#order").val(data.order);
	  	$("#weight").val(data.weight);
	  	$('#img').attr('src', "https://img.pokemondb.net/artwork/" + data.name + ".jpg");
	  	$('#name_title').html(data.name);
	}).always(function() {
    	$("#loading").hide();
	});
}

function abilities(abilities)
{
	var _abilities = "";
	
	for (var i=0; i < abilities.length; i++)		
		_abilities += abilities[i].ability.name + " ";

	return _abilities;
}

function types(types)
{
	var _types = "";
	
	for (var i=0; i < types.length; i++)
	{		
		_types += types[i].type.name + " ";
	}
	return _types;
}